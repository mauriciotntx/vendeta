<?php 

use \Illuminate\Database\Seeder;
use Vinder\Entities\User;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        User::create([
            'name'          => 'Súper Administrador',
            'username'      => 'superadmin',
            'email'         => 'superadmin@vinder.info',
            'password'      => 123,
            'type_id'       => 1,
            'created_at'    => new DateTime,
            'updated_at'    => new DateTime 
        ]);

        User::create([
            'name'          => 'Andrés Pinzón',
            'username'      => 'admin',
            'email'         => 'andrestntx@vinder.info',
            'password'      => 123,
            'type_id'       => 2,
            'created_at'    => new DateTime,
            'updated_at'    => new DateTime 
        ]);

        User::create([
            'name'          => 'Digitadora Ana Maria',
            'username'      => 'ana',
            'email'         => 'admin@vinder.info',
            'password'      => 123,
            'type_id'       => 3,
            'created_at'    => new DateTime,
            'updated_at'    => new DateTime 
        ]);
    }
}

?>