<?php namespace Vinder\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        View::composers([
            'Vinder\Http\ViewComposers\MenuComposer'        => ['auth.login',
                                                                'dashboard.pages.modules',
                                                                'dashboard.pages.database.hello',
                                                                'dashboard.pages.database.voters.diaries',
                                                                'dashboard.pages.database.voters.lists.layout',
                                                                'dashboard.pages.database.voters.forms.layout',
                                                                'dashboard.pages.database.roles.*',
                                                                'dashboard.pages.reports.*',
                                                                'dashboard.pages.statistics.*',
                                                                'dashboard.pages.diary.*',
                                                                'dashboard.pages.logistic.*',
                                                                'dashboard.pages.advertising.*',
                                                                'dashboard.pages.sms.*',
                                                                'dashboard.pages.polls.*',
                                                                'dashboard.pages.system.*'],
            'Vinder\Http\ViewComposers\DatabaseComposer'    => 'dashboard.pages.database.hello',

            /* Database */
            'Vinder\Http\ViewComposers\Voter\ListComposer'          => ['dashboard.pages.database.voters.lists.voter', 
                                                                        'dashboard.pages.database.voters.lists.team',
                                                                        'dashboard.pages.diary.people'],
            'Vinder\Http\ViewComposers\Voter\ListVoterComposer'     => ['dashboard.pages.database.voters.lists.voter',
                                                                        'dashboard.pages.database.voters.lists.search',
                                                                        'dashboard.pages.diary.people'],
            'Vinder\Http\ViewComposers\Voter\ListTeamComposer'      => 'dashboard.pages.database.voters.lists.team',
            'Vinder\Http\ViewComposers\Voter\PruebaComposer'          => ['dashboard.pages.database.voters.forms.voter', 
                                                                        'dashboard.pages.database.voters.forms.team'],
            'Vinder\Http\ViewComposers\Rol\FormComposer'            => 'dashboard.pages.database.roles.lists',

            /* System */
            'Vinder\Http\ViewComposers\Location\FormComposer'           => 'dashboard.pages.system.locations.form',
            'Vinder\Http\ViewComposers\PollingStation\FormComposer'     => 'dashboard.pages.system.polling_stations.form',
            'Vinder\Http\ViewComposers\Module\FormComposer'             => 'dashboard.pages.system.modules.form',
            'Vinder\Http\ViewComposers\Sms\FormComposer'                => 'dashboard.pages.system.sms.form',
            'Vinder\Http\ViewComposers\User\FormComposer'               => 'dashboard.pages.system.users.form',
            'Vinder\Http\ViewComposers\UserType\FormComposer'           => 'dashboard.pages.system.user_types.form',
            'Vinder\Http\ViewComposers\ReportStatistics\FormComposer'   => 'dashboard.pages.system.reports.form',

            /* Secundary */
            'Vinder\Http\ViewComposers\Diary\FormComposer'              => 'dashboard.pages.diary.form',
            'Vinder\Http\ViewComposers\Diary\ListComposer'              => ['dashboard.pages.logistic.hello',
                                                                            'dashboard.pages.advertising.hello'],
            'Vinder\Http\ViewComposers\Statistics\ListComposer'         => 'dashboard.pages.statistics.lists',
            'Vinder\Http\ViewComposers\Report\ListComposer'             => ['dashboard.pages.reports.lists',
                                                                            'dashboard.pages.statistics.lists'],

            /* Extra */
            'Vinder\Http\ViewComposers\Sms\ListComposer'                => 'dashboard.pages.sms.sms',
            'Vinder\Http\ViewComposers\Poll\ListComposer'               => 'dashboard.pages.polls.lists',
            'Vinder\Http\ViewComposers\Poll\OptionsComposer'            => 'dashboard.pages.polls.options'
        ]);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
