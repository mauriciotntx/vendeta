<?php namespace Vinder\Http\Controllers\System;

use Vinder\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SystemController extends Controller {

	public function getIndex()
	{
		return view('dashboard.pages.system.hello');
	}

}
