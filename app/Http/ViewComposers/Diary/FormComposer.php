<?php namespace Vinder\Http\ViewComposers\Diary;

use Illuminate\Contracts\View\View;
use Auth;

use Vinder\Entities\Location;
use Vinder\Entities\Voter;

class FormComposer {
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $locations    = Location::getAllOrder();
        $delegates    = Voter::allDelegates();
        $team         = Voter::allTeam();

        return $view->with([
            'locations' => $locations,
            'delegates' => $delegates,
            'team'      => $team
        ]);
    }
}
