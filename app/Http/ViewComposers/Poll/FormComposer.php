<?php namespace Vinder\Http\ViewComposers\Voter;

use Illuminate\Contracts\View\View;
use Auth;

use Vinder\Entities\Voter;
use Vinder\Entities\Location;
use Vinder\Entities\Community;
use Vinder\Entities\Occupation;
use Vinder\Entities\Rol;

class FormComposer {
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
		$locations 			= Location::getAllOrder(1);
        $communities        = Community::allLists();

		$view->with([
            'locations'     => $locations
        ]);
    }
}


