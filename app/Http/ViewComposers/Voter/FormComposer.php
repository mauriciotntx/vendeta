<?php namespace Vinder\Http\ViewComposers\Voter;

use Illuminate\Contracts\View\View;
use Auth;

use Vinder\Entities\Voter;
use Vinder\Entities\Location;
use Vinder\Entities\Community;
use Vinder\Entities\Occupation;
use Vinder\Entities\Rol;
use Vinder\Entities\PollingStation;

class FormComposer {
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {        
        //dd('si');
        $team               = Voter::allTeam();
		$locations 			= Location::getAllOrder(1);
		$communities 		= Community::allLists();
		$occupations 		= Occupation::allLists();
        $roles              = Rol::allLists();
        $polling_stations   = PollingStation::allLists();

		$view->with([
            'team'          => $team,
            'locations'     => $locations, 
            'communities'   => $communities, 
            'occupations'   => $occupations,
            'roles'         => $roles,
            'polling_stations'  => $polling_stations
        ]);
    }
}


