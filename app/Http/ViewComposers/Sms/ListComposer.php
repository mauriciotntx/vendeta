<?php namespace Vinder\Http\ViewComposers\Sms;

use Illuminate\Contracts\View\View;
use Auth;

use Vinder\Libraries\Sms\SendSMS;
use Vinder\Entities\Location;
use Vinder\Entities\PollingStation;
use Vinder\Entities\Community;
use Vinder\Entities\Occupation;
use Vinder\Entities\Rol;

class ListComposer {
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $sms = new SendSMS();

        $credits 	= $sms->credits();
		

        return $view->with([
            'credits'           => $credits,
            'locations'         => Location::getAllOrder(),
            'polling_stations'  => PollingStation::allLists(),
            'communities'       => Community::allLists(),
            'roles'             => Rol::allLists(),
            'occupations'       => Occupation::allLists(),
            'sex'               => ['F' => 'Femenino', 'M' => 'Masculino']            
        ]);
    }
}


